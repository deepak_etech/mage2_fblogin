<?php

class Excellence_Facebooklogin_Model_Observer extends Varien_Object {

    function loginButton($observer) {

    $layoutName=$observer->getEvent()->getBlock()->getNameInLayout();
     if($layoutName=='customer_form_login'){
       $transport = $observer->getTransport();
       $html = $transport->getHtml();

        $postHtml=$observer->getEvent()
         ->getBlock()
         ->getLayout()
         ->createBlock('core/template')
         ->setTemplate('facebooklogin/login.phtml')
         ->toHtml();
      
      $html = $postHtml.$html;
      $transport->setHtml($html);    
     } 
     if($layoutName=='customer_form_register')
     {
        $transport = $observer->getTransport();
       $html = $transport->getHtml();

        $postHtml=$observer->getEvent()
         ->getBlock()
         ->getLayout()
         ->createBlock('core/template')
         ->setTemplate('facebooklogin/register.phtml')
         ->toHtml();
      
      $html = $postHtml.$html;
      $transport->setHtml($html); 
     }


     
  }
}
