<?php 

class Excellence_Facebooklogin_AccountController extends Mage_Core_Controller_Front_Action {

    public function passwordAction()
    {
        $this->loadLayout();
    	$this->renderLayout();
     }

     public function logoutAction()
    { 
        $this->loadLayout();
        $this->renderLayout();
    }

    public function removeidAction(){
       $customerId=Mage::app()->getRequest()->getParam('customer_id');
       $model=Mage::getModel('customer/customer')->load($customerId);
       $model->setFacebookId("");
       $model->save();
       Mage::getSingleton('core/session')->addSuccess("Your facebook account removed successfully.");
       $this->_redirectReferer();
    }

    public function loginAction(){

    	$params=Mage::app()->getRequest()->getParams();
       $url_redirect=$params['http_return_url'];
        $websiteId = Mage::app()->getWebsite()->getId();
        $store = Mage::app()->getStore();
        $customer = Mage::getModel("customer/customer");
        $customer->website_id = $websiteId;
        $customer->setStore($store);
        $customerload = $customer->loadByEmail($params['email']);
        $customerload->setFacebookId($params["facebook_id"]);
        $customerload->save();

        $session=Mage::getSingleton('customer/session');
     
       try {
            $session->login($params['email'], $params['password']);
            $session->setCustomerAsLoggedIn($session->getCustomer());
            $session->addSuccess(
            $this->__('Thank you for login with %s.', Mage::app()->getStore()->getFrontendName())
            );
            Mage::getSingleton('core/session')->unsSessionVariable();
            if($url_redirect==''){
              $this->_redirect('customer/account/index');
            } else {
               $this->_redirect("checkout/onepage");
            }
            
            
            } catch (Exception $e) {
               
               Mage::getSingleton('core/session')->addError($e->getMessage());    // PA DSS violation: this exception log can disclose customer password
               $this->_redirect('*/*/password');
           }  
       
        
     
    }
}  