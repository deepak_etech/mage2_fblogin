<?php


class Excellence_Facebooklogin_CustomerController extends Mage_Core_Controller_Front_Action {

    public function LoginAction()
    {

        $facebookResponse = null;

        $cookie = $this->get_facebook_cookie(Mage::getStoreConfig('facebooklogin/settings/appid'), Mage::getStoreConfig('facebooklogin/settings/secret'));

        $facebookResponse = json_decode($this->getFbData('https://graph.facebook.com/me?locale=en_US&fields=name,email,first_name,last_name,locale&access_token=' . $cookie['access_token']));
     
        $urlRedirect=$_SERVER['HTTP_REFERER'];

        if (!is_null($facebookResponse)) {
		    	$facebookResponse = (array)$facebookResponse;
         
            $session = Mage::getSingleton('customer/session');

              $collection = Mage::getModel('customer/customer')
              ->getCollection()
              ->addAttributeToSelect('facebook_id')
              ->addAttributeToFilter('facebook_id',$facebookResponse['id'])->load();
           
           
             if (!$collection->getData() || empty($collection->getData())) {
                if (array_key_exists("email",$facebookResponse))
                  {
                      $collectionEmail=Mage::getResourceModel('customer/customer_collection')
                             ->addAttributeToFilter('email',$facebookResponse['email']);
                      
                       if (!$collectionEmail->getData() || empty($collectionEmail->getData())) {
                           $this->_registerCustomer($facebookResponse, $session);
                       }else{
                          $facebookResponse['http_referer']=$urlRedirect;
                          Mage::getSingleton('core/session')->setSessionVariable($facebookResponse);
                          $this->_redirect('facebooklogin/account/password');
                       }
                  }
                else
                  {
                 Mage::getSingleton('core/session')->addError("Couldn't get email from your facebook account. Please check your privacy settings");  
                    if (strpos($urlRedirect, 'checkout/') !== false) {
                    Mage::app()->getResponse()->setRedirect($urlRedirect);
                    Mage::app()->getResponse()->sendResponse();
                    } else {
                       $this->_redirect('customer/account/index');
                    }  
                  }
                  

              } else {
             
             foreach($collection as $col){
             $email=$col->getEmail();
              }
         
            $websiteId = Mage::app()->getWebsite()->getId();
            $store = Mage::app()->getStore();
            $customer = Mage::getModel("customer/customer");
            $customer->website_id = $websiteId;
            $customer->setStore($store);
   
                   
            try {
                $customer->loadByEmail($email);
                $session = Mage::getSingleton('customer/session')->setCustomerAsLoggedIn($customer);
                $session->addSuccess(
               $this->__('Thank you for login with %s.', Mage::app()->getStore()->getFrontendName())
               );
           
             if (strpos($urlRedirect, 'checkout/') !== false) {
                  Mage::app()->getResponse()->setRedirect($urlRedirect);
                  Mage::app()->getResponse()->sendResponse();
                  } else {
                     $this->_redirect('customer/account/index');
                  }  

               
            
            } catch (Exception $e) {
                     Mage::logException($e);
                }  

               
              }
           }      

    }


   

    private function _registerCustomer($data, &$session)
    {  
        $customer = Mage::getModel('customer/customer')->setId(null);
        $customer->setData('firstname', $data['first_name']);
        $customer->setData('lastname', $data['last_name']);
        $customer->setData('email', $data['email']);
        $customer->setData('facebook_id', $data['id']);
        $customer->setData('password', md5(time() . $data['id'] . $data['locale']));
        $customer->setData('is_active', 1);
        $customer->setData('confirmation', null);
        $customer->setConfirmation(null);
        $customer->getGroupId();
        $customer->save();

        Mage::getModel('customer/customer')->load($customer->getId())->setConfirmation(null)->save();
        $customer->setConfirmation(null);
        $session->setCustomerAsLoggedIn($customer);
        $customer_id = $session->getCustomerId();
        $session->addSuccess(
           $this->__('Thank you for login with %s.', Mage::app()->getStore()->getFrontendName())
         );
        $urlRedirect=$_SERVER['HTTP_REFERER'];
        if (strpos($urlRedirect, 'checkout/') !== false) {
                    Mage::app()->getResponse()->setRedirect($urlRedirect);
                    Mage::app()->getResponse()->sendResponse();
                    } else {
                       $this->_redirect('customer/account/index');
          } 
       
    }

  
    private function get_facebook_cookie($app_id, $app_secret)
    { 

        if ($_COOKIE['fbsr_' . $app_id] != '') {
            return $this->get_new_facebook_cookie($app_id, $app_secret);
        } else {
            return $this->get_old_facebook_cookie($app_id, $app_secret);
        }
    }

    private function get_old_facebook_cookie($app_id, $app_secret)
    {
        $args = array();
        parse_str(trim($_COOKIE['fbs_' . $app_id], '\\"'), $args);
        ksort($args);
        $payload = '';
        foreach ($args as $key => $value) {
            if ($key != 'sig') {
                $payload .= $key . '=' . $value;
            }
        }
        if (md5($payload . $app_secret) != $args['sig']) {
            return array();
        }
        return $args;
    }

    private function get_new_facebook_cookie($app_id, $app_secret)
    {
      $signed_request = $this->parse_signed_request($_COOKIE['fbsr_' . $app_id], $app_secret);
       
        // $signed_request should now have most of the old elements
        $signed_request['uid'] = $signed_request['user_id']; // for compatibility 
      if (!is_null($signed_request)) {
            // the cookie is valid/signed correctly
            // lets change "code" into an "access_token"
			$access_token_response = $this->getFbData("https://graph.facebook.com/oauth/access_token?client_id=$app_id&redirect_uri=&client_secret=$app_secret&code=$signed_request[code]");
			parse_str($access_token_response);
			$signed_request['access_token'] = $access_token;
			$signed_request['expires'] = time() + $expires;
        }

        return $signed_request;
    }

    private function parse_signed_request($signed_request, $secret)
    {
        list($encoded_sig, $payload) = explode('.', $signed_request, 2);

        // decode the data
        $sig = $this->base64_url_decode($encoded_sig);
        $data = json_decode($this->base64_url_decode($payload), true);

        if (strtoupper($data['algorithm']) !== 'HMAC-SHA256') {
            error_log('Unknown algorithm. Expected HMAC-SHA256');
            return null;
        }

        // check sig
        $expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
        if ($sig !== $expected_sig) {
            error_log('Bad Signed JSON signature!');
            return null;
        }

        return $data;
    }

    private function base64_url_decode($input)
    {
        return base64_decode(strtr($input, '-_', '+/'));
    }
	
	private function getFbData($url)
	{
		$data = null;

		if (ini_get('allow_url_fopen') && function_exists('file_get_contents')) {
			$data = file_get_contents($url);
		} else {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$data = curl_exec($ch);
		}
		return $data;
	}

}
