<?php

class Excellence_Facebooklogin_Block_Active extends Mage_Core_Block_Template {

    public function getAppId()
    {
        return Mage::getStoreConfig('facebooklogin/settings/appid');
    }

    public function getSecretKey()
    {
        return Mage::getStoreConfig('facebooklogin/settings/secret');
    }

    public function isActiveLike()
    {
        return Mage::getStoreConfig('facebooklogin/like/enabled');
    }

   

 	
}
