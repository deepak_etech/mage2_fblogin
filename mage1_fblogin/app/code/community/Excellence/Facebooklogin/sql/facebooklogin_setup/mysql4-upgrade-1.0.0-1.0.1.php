<?php

$installer = $this;

$installer->startSetup();

$attribute  = array(
    'type' => "varchar",
    'input' => 'text',
    'label' => 'Is Active',
    'global' => 1,
    'visible' => 1,
    'default' => '0',
    'required' => 0,
    'user_defined' => 0,
    'used_in_forms' => array(
        'adminhtml_customer',
    ),
    'comment' => 'customer custom attribute facebook_id',
); 

$installer->addAttribute('customer', 'facebook_id', $attribute);

 Mage::getSingleton('eav/config')
    ->getAttribute('customer', 'facebook_id')
    ->setData('used_in_forms', array('adminhtml_customer'))
    ->save();

$installer->endSetup();


