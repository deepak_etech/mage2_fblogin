<?php


class Excellence_Facebooklogin_Helper_Active extends Mage_Core_Helper_Abstract {

    public function getAppId() {
        return Mage::getStoreConfig('facebooklogin/settings/appid');
    }

    public function getSecretKey() {
        return Mage::getStoreConfig('facebooklogin/settings/secret');
    }

   

    public function getLoginImg() {
   
        $img = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN) .
                    'frontend/base/default/images/facebooklogin/fb.gif';
      
        return $img;
    }

    public function isActiveActivity()
    {
        return Mage::getStoreConfig('facebooklogin/activity/enabled');
    }        
  
        
}
